# Installation

    git clone https://gitlab.ethz.ch/spacegeodesy_public/vlbi_jet_angle.git
    cd vlbi_jet_angle
    python -m venv venv 
    source venv/bin/activate 
    pip install -r requirements.txt 
    # test installation
    python main.py test
    # check if plots/list was generated in ./test

# Help 

    python main.py -h 

