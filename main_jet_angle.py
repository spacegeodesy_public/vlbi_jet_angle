import argparse
from collections import defaultdict
from itertools import combinations
from math import pi
from pathlib import Path

import matplotlib
import numpy as np
import pandas as pd
import tabulate
from astropy.time import Time

import skd_parser.skd as skd_parser

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

DEG2RAD = pi / 180
RAD2DEG = 180 / pi

FIGURES = dict()
LIST = pd.DataFrame()


def run(skd_file, generate_plots, generate_lists) -> None:
    """
    Process a single skd file
    :param skd_file: path to skd file
    :param generate_plots: flag if plots will be generated
    :param generate_lists: flag if list will be generated
    :return: None
    """
    skd = skd_parser.skdParser(skd_file)
    skd.parse()
    print(f"processing{skd_file.name} ({' '.join([sta.name2 for sta in skd.stations])})")

    sources = defaultdict(list)
    for scan in skd.scans.scans:
        time = Time(scan.start_time, scale='utc')
        gmst = time.sidereal_time('mean', 'greenwich')
        srcname = scan.source.name
        ra = scan.source.ra
        de = scan.source.de
        for a, b in combinations(scan.observations.obs, 2):
            dx = a.station.x - b.station.x
            dy = a.station.y - b.station.y
            dz = a.station.z - b.station.z
            uv = calc_uv(gmst, ra, de, dx, dy, dz)
            sources[srcname].append((time, a.station.name2, b.station.name2, *uv))
            pass
        pass

    if generate_plots:
        for name, uv in sources.items():
            uvplot(name, uv)
    if generate_lists:
        for name, uv in sources.items():
            uvlist(skd_file, name, uv)


def calc_uv(gmst, ra, de, dx, dy, dz) -> tuple[float, float]:
    """
    calculate UV projection of baseline

    :param gmst: greenwich mean sidereal time
    :param ra: right ascension [rad]
    :param de: declination [rad]
    :param dx: delta x [m]
    :param dy: delta y [m]
    :param dz: delta z [m]
    :return: (u,v) [m, m]
    """
    gmst = gmst.value * 15 * DEG2RAD
    ha = gmst - ra

    sinHa = np.sin(ha)
    cosHa = np.cos(ha)
    cosDe = np.cos(de)
    sinDe = np.sin(de)

    u = dx * sinHa + dy * cosHa
    v = dz * cosDe + sinDe * (-dx * cosHa + dy * sinHa)
    return u, v


def uvplot(name, uv) -> None:
    """
    generate or append basic UV-coverage plot

    :param name: source name
    :param uv: list of UV projections
    :return: None
    """
    if name not in FIGURES:
        fig, ax = plt.subplots(1, 1, figsize=(5, 5))
        FIGURES[name] = (fig, ax)

        plt.xlabel('u [1000 km]')
        plt.ylabel('v [1000 km]')
        plt.xlim([-13, 13])
        plt.ylim([-13, 13])
        plt.grid(True, which='major', color='gray', alpha=0.5)
        plt.grid(True, which='minor', color='gray', linestyle=":", alpha=0.25)
        plt.title(name)
        plt.tight_layout()
    else:
        fig, ax = FIGURES[name]
        plt.figure(fig)
        plt.sca(ax)

    ax.set_aspect('equal')
    ax.minorticks_on()

    time, sta1, sta2, u, v = zip(*uv)
    u = np.asarray(u)
    v = np.asarray(v)

    circle = plt.Circle((0, 0), 2 * 6.371, color="gray", fill=False)
    ax.add_patch(circle)

    plt.scatter(u / 1e6, v / 1e6, 10, 'k')
    plt.scatter(-u / 1e6, -v / 1e6, 10, 'k')
    pass


def uvlist(session, source, uv) -> None:
    """
    generate or append list of UV projections and angles

    :param session: path to skd file
    :param source: source name
    :param uv: tuple containing (time, sta1, sta2, u, v) [-, -, -, m, m]
    :return: None
    """
    time, sta1, sta2, u, v = zip(*uv)
    df = pd.DataFrame({"session": session.name, "time": time, "sta1": sta1, "sta2": sta2, "source": source,
                       "u": u, "v": v, "angle": np.nan, "jet_angle": np.nan})
    df.angle = np.rad2deg(np.arctan(df.u / df.v))
    global LIST
    df = pd.concat([LIST, df], ignore_index=True)
    LIST = df


def make_plots(outdir, sources) -> None:
    """
    generate plots in outdir/output

    :param outdir: output directory
    :param sources: source list
    :return: None
    """

    outdir = outdir / "output"
    outdir.mkdir(exist_ok=True)

    def arc_patch(theta1, theta2, ax=None, **kwargs):
        if ax is None:
            ax = plt.gca()
        r = 2 * 6.371
        theta = np.linspace(np.radians(theta1), np.radians(theta2), 50)
        points = np.vstack((r * np.cos(theta), r * np.sin(theta)))
        points = np.hstack([points, np.array([[0], [0]])])

        poly = mpatches.Polygon(points.T, closed=True, **kwargs)
        ax.add_patch(poly)
        return poly

    def plot_angle(angle, **kwargs):
        angle = np.deg2rad(angle)
        x = 2 * 6.371 * np.cos(angle)
        y = 2 * 6.371 * np.sin(angle)
        plt.plot([-x, x], [-y, y], **kwargs)

    for name, (fig, ax) in FIGURES.items():
        plt.figure(fig)
        plt.sca(ax)
        src = sources[sources.name == name]
        if src.shape[0] == 0:
            src = sources[sources.name2 == name]
        orig_angle = src["ja"].values[0]
        angle = 90 - src["ja"].values[0]

        plot_angle(angle, color="red")
        plot_angle(angle + 45, color="red", linestyle="--")
        plot_angle(angle - 45, color="red", linestyle="--")
        arc_patch(angle - 45, angle + 45, ax=ax, fill=True, color='red', alpha=0.2)
        arc_patch(180 + angle - 45, 180 + angle + 45, ax=ax, fill=True, color='red', alpha=0.2)
        plt.title(f"{name} JA: {orig_angle:.0f} deg")

        plt.savefig(outdir / f'{name}.png', dpi=150)
        plt.close(fig)
        pass
    pass


def make_list(outdir, sources) -> None:
    """
    generate list in outdir/output/list.csv

    :param outdir: output directory
    :param sources: source list
    :return: None
    """
    outdir = outdir / "output"
    outdir.mkdir(exist_ok=True)
    df = LIST.sort_values("time")
    for _, source in sources.iterrows():
        df.loc[df["source"] == source["name"], "jet_angle"] = source["ja"]
    with open(outdir / "list.csv", "w") as f:
        f.write(tabulate.tabulate(df, headers="keys", tablefmt="plain", showindex=False,
                                  floatfmt=("", "", "", "", "", ".2f", ".1f", ".2f", ".2f")))
    pass


if __name__ == "__main__":
    doc = "Process all .skd files and generate UV-coverge plots as well as list of UV vs. jet angles. " \
          "All .skd files in the given folder will be processed together." \
          "Output will be stored in the same folder as the input .skd files under under '/output/'."
    parser = argparse.ArgumentParser(description=doc)
    parser.add_argument("folder", help="path to folder with skd files (default = ./test)", default="./test")
    parser.add_argument("-s", "--source_cat", help="path to source catalog (default = CATALOGS/source.cat.jet)",
                        default="CATALOGS/source.cat.jet")
    parser.add_argument("-np", "--no-plot", action="store_true", help="generate plots")
    parser.add_argument("-nl", "--no-list", action="store_true", help="generate list")
    args = parser.parse_args()

    sources = pd.read_csv(args.source_cat, comment="*", sep="\s+", header=None,
                          names="name name2 ra_h ra_m ra_s de_d de_m de_s j1 j2 s1 s2 s3 ja ja_sigma".split())

    generate_plots = not args.no_plot
    generate_list = not args.no_list

    for skd in Path(args.folder).glob("*.skd"):
        print(f"processing {skd}: ", end="")
        run(skd, generate_plots, generate_list)
    if args.generate_plots:
        make_plots(Path(args.folder), sources)
    if args.generate_list:
        make_list(Path(args.folder), sources)
    FIGURES = dict()
