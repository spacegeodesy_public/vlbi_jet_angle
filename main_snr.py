import argparse
from pathlib import Path
import subprocess
import re

def clean_snr(file):
    clean_snr = []
    with open(file) as f:
        for l in f:
            tmp = l.split()
            if len(tmp) == 14:
                if (tmp[6] == "S" or tmp[6] == "X"):
                    continue

            clean_snr.append(l)
    with open(file, "w") as f:
        f.write("".join(clean_snr))


if __name__ == "__main__":
    doc = "Process all .skd files and generate *.snr files " \
          "Output will be stored in the same folder as the input .skd files under under '/snr/'."

    parser = argparse.ArgumentParser(description=doc)
    parser.add_argument("folder", help="path to folder with skd files (default = ./test)", default="./test")
    parser.add_argument("scheduler", help="path to VieSched++ executable")
    args = parser.parse_args()

    # validate arguments
    scheduler = Path(args.scheduler).absolute()
    if not Path(scheduler).is_file():
        raise FileNotFoundError(f"VieSched++ not found. Check path {scheduler}")
    if not Path(args.folder).is_dir():
        raise FileNotFoundError(f"Path to .skd files not found. Check {args.folder}")

    outdir = Path(args.folder) / "snr"
    outdir.mkdir(exist_ok=True)

    # pattern in .skd file that will be replaced
    re_oe = re.compile(f"T\s+Oe\s+ONSA13NE\s.*")
    re_ow = re.compile(f"T\s+Ow\s+ONSA13SW\s.*")
    re_is = re.compile(f"T\s+Is\s+ISHIOKA\s.*")

    # strings to replace pattern with
    vgos_oe = "T Oe ONSA13NE   2x56000  17640  A  2800.0   B  1700.0  C  1600.0   D  2200.0   DBBC_DDC FlexBuff"
    vgos_ow = "T Ow ONSA13SW   2x56000  17640  A  2400.0   B  1600.0  C  1500.0   D  1600.0   DBBC_DDC FlexBuff"
    vgos_is = "T Is ISHIOKA     1       100    A  2400.0   B  3600.0  C  3900.0   D  5100.0   K4-2 K5"

    for file in Path(args.folder).glob("*.skd"):
        print(f"processing {file}: ")
        with open(file) as f:
            skd = f.read()

        # replace SX-SEFD information wiht VGOS
        skd = re_oe.sub(vgos_oe, skd)
        skd = re_ow.sub(vgos_ow, skd)
        skd = re_is.sub(vgos_is, skd)

        # generate dummy VGOS .skd file
        vgos = outdir / f"{file.stem}_VGOS.skd"
        with open(vgos, "w") as f:
            f.write(skd)

        # generate .snr file
        out = subprocess.run([str(scheduler.absolute()), "--snr", str(vgos.absolute())],
                             capture_output=True, text=True, cwd=str(vgos.parent.absolute()))
        if out.stderr:
            raise Exception(f"{out.stderr}")

        # clean .snr file
        snr = vgos.parent / f"{vgos.stem}.snr"
        clean_snr(snr)

        vgos.unlink()